package alpha;
import battlecode.common.*;

public strictfp class Lumberjack {
	
	public static boolean KILLMODE = false;
	public static MapLocation enemyLocation;
	public static Team enemyTeam = RobotPlayer.rc.getTeam().opponent();
	
	public static void runLumberjack() throws GameActionException {
		
		while (true) {
			
			try {
				// KILLMODE is when the coordinates of an enemy is in the broadcast array
				if (KILLMODE){
			
					// If Lumberjack senses nearby enemy, attack
					if (RobotPlayer.rc.senseNearbyRobots(4, enemyTeam).length != 0){
						RobotPlayer.rc.strike();
					}
					
					// If first slot in broadcast array is 0 again, turn off KILLMODE
					if (RobotPlayer.rc.readBroadcast(0) == 0){
						KILLMODE = false;
					}
					
					// Else, keep moving toward enemy location
					else {
						// If lumberjack is at position and there are no enemies, turn off KILLMODE
						if (RobotPlayer.rc.getLocation().equals(enemyLocation)){
							KILLMODE = false;
							RobotPlayer.rc.broadcast(0, 0);
						}
						if (RobotPlayer.rc.canMove(enemyLocation)){
							RobotPlayer.rc.move(enemyLocation);
						}
						else {
							RobotPlayer.rc.move(randomDirection());
						}
					}
					
				}
				
				// No KILLMODE
				else{
					// If lumberjacks write 9 to 1st array slot, the next 2 slots will be coordinates of an enemy
					if (RobotPlayer.rc.readBroadcast(0) == 9){
						KILLMODE = true;
						enemyLocation = new MapLocation((float)RobotPlayer.rc.readBroadcast(1), (float)RobotPlayer.rc.readBroadcast(2));
					}
					
					// KILLMODE not activated by another lumberjack
					else {
						RobotInfo[] potentialEnemies = RobotPlayer.rc.senseNearbyRobots(7, enemyTeam);
						// Check for enemies, and signal other lumberjacks if there are.
						if (potentialEnemies.length > 0) {
							enemyLocation = potentialEnemies[0].getLocation();
							RobotPlayer.rc.broadcast(0, 9);
							RobotPlayer.rc.broadcast(1, (int)enemyLocation.x);
							RobotPlayer.rc.broadcast(2, (int)enemyLocation.y);
							KILLMODE = true;
							continue;
						}
						
						else{
							
							/*
							TreeInfo[] neutralTrees = RobotPlayer.rc.senseNearbyTrees(2, Team.NEUTRAL);
							// Attack nearby neutral trees if there are no enemies nearby
							if (neutralTrees.length > 0){
								enemyLocation = neutralTrees[0].getLocation();
								RobotPlayer.rc.broadcast(0, 9);
								RobotPlayer.rc.broadcast(1, (int)enemyLocation.x);
								RobotPlayer.rc.broadcast(2, (int)enemyLocation.y);
								KILLMODE = true;
								continue;
							}
							*/
							
							// If there are no enemies, move randomly
							Direction dir = randomDirection();
							while (!(RobotPlayer.rc.canMove(dir))){
								dir = randomDirection();
							}
							RobotPlayer.rc.move(dir);
						}
					}
				}
				
			}
			
			catch (Exception e) {
				System.out.println("Lumberjack unhandled exception occured");
				e.printStackTrace();
			}
			
		}
		
	}
	
	
	static Direction randomDirection() {
        return new Direction((float)Math.random() * 2 * (float)Math.PI);
    }
	
	
}