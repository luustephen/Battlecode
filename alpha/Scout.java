package alpha;
import battlecode.common.*;

import java.util.Arrays;
import java.util.Random;

public strictfp class Scout {
	
	public static RobotInfo[] enemies;
	public static TreeInfo[] trees;
	public static Team team = RobotPlayer.rc.getTeam();
	public static MapLocation location;
	public static MapLocation archonLocation = RobotPlayer.rc.getInitialArchonLocations(team.opponent())[0];
	public static boolean shouldMove = true;
	public static Direction moveDirection = randomDirection();
	
	public static boolean attackMode = false;
	
	
	public static void runScout() throws GameActionException {
		
		
		while (true) {
			
			try {
				
				if (attackMode){
					
					// Attack any nearby enemies, only if you can move toward them (to stop friendly fire)
					RobotInfo[] nearbyEnemies = RobotPlayer.rc.senseNearbyRobots(10, team.opponent());
					if (nearbyEnemies.length > 0){
						if (RobotPlayer.rc.canMove(nearbyEnemies[0].getLocation())){
							if (RobotPlayer.rc.canFireSingleShot()){
								RobotPlayer.rc.fireSingleShot(new Direction(RobotPlayer.rc.getLocation(), nearbyEnemies[0].getLocation()));
							}
							RobotPlayer.rc.move(nearbyEnemies[0].getLocation());
						}
					}
					
					// Otherwise, try to move toward initial enemy archon location
					else if (RobotPlayer.rc.canMove(archonLocation)){
						RobotPlayer.rc.move(archonLocation);
					}
					else {
						Direction dir = randomDirection();
						if (RobotPlayer.rc.canMove(dir)){
							RobotPlayer.rc.move(dir);
						}
						else {
							continue;
						}
					}
				}
				
				
				else {
					
					// Attack any nearby enemies
					RobotInfo[] nearbyEnemies = RobotPlayer.rc.senseNearbyRobots(4, team.opponent());
					if (nearbyEnemies.length > 0){
						if (RobotPlayer.rc.canFireSingleShot()){
							RobotPlayer.rc.fireSingleShot(new Direction(RobotPlayer.rc.getLocation(), nearbyEnemies[0].getLocation()));
						}
					}
					
					// Move out of the way if there are robots very close
					while (RobotPlayer.rc.senseNearbyRobots(2, team).length > 0){
						if (RobotPlayer.rc.canMove(new Direction(RobotPlayer.rc.getLocation(), archonLocation))){
							RobotPlayer.rc.move(new Direction(RobotPlayer.rc.getLocation(), archonLocation));
						}
						else {
							Direction dir = randomDirection();
							if (RobotPlayer.rc.canMove(dir)){
								RobotPlayer.rc.move(dir);
							}
						}
					}
					
				}
				
				// Set attackMode after round 400
		        int round = RobotPlayer.rc.getRoundNum();
		        if (round > 400){
		        	attackMode = true;
		        }
		        
				
			}
			
			catch (Exception e) {
				System.out.println("Scout unhandled exception occured");
				e.printStackTrace();
			}	
		}
	}

	static Direction randomDirection() {
        return new Direction((float)Math.random() * 2 * (float)Math.PI);
    }
	
	
	
}