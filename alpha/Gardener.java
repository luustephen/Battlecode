package alpha;
import battlecode.common.*;
public strictfp class Gardener {
	
	public static boolean soldierMode = false;
	public static boolean treeMode = false;
	public static boolean waterMode = false;
	public static boolean panicMode = false;
	public static boolean yieldMode = false;
	public static boolean spawnedSoldier = false;
	public static Team team = RobotPlayer.rc.getTeam();
	
	public static int roundSpawn = RobotPlayer.rc.getRoundNum();
	
	public static int numTanks = 0;
	public static int numTrees = 0;
	public static int treeModeRound = 0;
	public static int walkModeCounter = 0;
	
	public static Direction moveDirection = Direction.getSouth();
	
	public static MapLocation archonLocation = RobotPlayer.rc.getInitialArchonLocations(team.opponent())[0];
	
	public static void runGardener() throws GameActionException {
		
		while (true) {
			
			try {
				
				/*
				if (panicMode && !(spawnedSoldier)){
					
					if (RobotPlayer.rc.canBuildRobot(RobotType.SCOUT, Direction.getEast())){
						RobotPlayer.rc.buildRobot(RobotType.SCOUT, Direction.getEast());
						RobotPlayer.rc.broadcast(3, 0);
						panicMode = false;
						spawnedSoldier = true;
						continue;
					}
					else {
						RobotPlayer.rc.setIndicatorDot(RobotPlayer.rc.getLocation(), 255, 255, 255);
					}
					
					if (RobotPlayer.rc.canMove(moveDirection)){
						RobotPlayer.rc.move(moveDirection);
						continue;
					}
					
					else{
						moveDirection = moveDirection.rotateLeftDegrees(70 + (int)(Math.random() * 10));
					}
				}
				*/
				
				
				int ss = RobotPlayer.rc.readBroadcast(0);
				if (ss < 2){
					soldierMode = true;
				}
				else {
					soldierMode = false;
				}
				
				if (soldierMode){
					if (RobotPlayer.rc.canBuildRobot(RobotType.SOLDIER, Direction.getEast())){
						RobotPlayer.rc.buildRobot(RobotType.SOLDIER, Direction.getEast());
						RobotPlayer.rc.broadcast(0, ss+1);
						soldierMode = false;
						continue;
					}
					else {
						
						if (RobotPlayer.rc.canMove(moveDirection)){
							RobotPlayer.rc.move(moveDirection);
							continue;
						}
						
						else{
							moveDirection = moveDirection.rotateRightDegrees(70 + (int)(Math.random() * 10));
							continue;
						}						
					}
				}
				
				
				
				/*
				// yieldMode -- stop planting trees and wait.  Water nearby trees in the meantime
				else if (yieldMode){
					if (RobotPlayer.rc.readBroadcast(3) == 0) {
						yieldMode = false;
					}
					
					// Check for panicMode
					RobotInfo[] nearbyEnemies = RobotPlayer.rc.senseNearbyRobots(7, team.opponent());
					if (nearbyEnemies.length > 0){
						panicMode = true;
						RobotPlayer.rc.broadcast(3, 1);
						continue;
					}
					
					// Water tree with least health
					int treeWithLeastHealthID = findWeakestTree(RobotPlayer.rc.senseNearbyTrees(2)).getID();
					if (RobotPlayer.rc.canWater(treeWithLeastHealthID)){
						RobotPlayer.rc.water(treeWithLeastHealthID);
					}
				}
				*/
				
				
				
				if (waterMode) {
					
					/*
					// Check for panic mode
					RobotInfo[] nearbyEnemies = RobotPlayer.rc.senseNearbyRobots(7, team.opponent());
					for (RobotInfo enemy : nearbyEnemies){
						if (!(enemy.type.equals(RobotType.ARCHON)) && !(enemy.type.equals(RobotType.GARDENER))){
							panicMode = true;
							RobotPlayer.rc.broadcast(3, 1);
							continue;
						}
					}
					*/
					
					// Water tree with least health
					int treeWithLeastHealthID = findWeakestTree(RobotPlayer.rc.senseNearbyTrees(2)).getID();
					if (RobotPlayer.rc.canWater(treeWithLeastHealthID)){
						RobotPlayer.rc.water(treeWithLeastHealthID);
					}
				}
				
				
				// TREEMODE ========================================================================================
				else if (treeMode){
					
					// Keep track of the first round that robot enters TreeMode
					if (treeModeRound == 0){
						treeModeRound = RobotPlayer.rc.getRoundNum();
					}
					
					/*
					// Check for panicMode
					RobotInfo[] nearbyEnemies = RobotPlayer.rc.senseNearbyRobots(7, team.opponent());
					for (RobotInfo enemy : nearbyEnemies){
						if (!(enemy.type.equals(RobotType.ARCHON)) && !(enemy.type.equals(RobotType.GARDENER))){
							panicMode = true;
							RobotPlayer.rc.broadcast(3, 1);
							continue;
						}
					}
					
					
					// Do not plant any trees if another robot is in panic mode
					if (RobotPlayer.rc.readBroadcast(3) > 0) {
						yieldMode = true;
						continue;
					}
					*/
					
					// Attempt to plant trees in all 6 directions 
					if (numTrees < 6){
						for (int i = 0; i < 6; i++){
							if (RobotPlayer.rc.canPlantTree(Direction.getEast().rotateLeftDegrees(i*60))) {
								RobotPlayer.rc.plantTree(Direction.getEast().rotateLeftDegrees(i*60));
								numTrees++;
							}
						}
					}
					
					else {
						waterMode = true;
					}
					
					// If it takes too long to plant trees, just start watering
					if (RobotPlayer.rc.getRoundNum() - treeModeRound > 160){
						waterMode = true;
					}
					
				}
				// ================================================================================================
				
				
				
				// WALKMODE========================================================================================
				else {
					
					/*
					// Check for panicMode
					RobotInfo[] nearbyEnemies = RobotPlayer.rc.senseNearbyRobots(7, team.opponent());
					for (RobotInfo enemy : nearbyEnemies){
						if (!(enemy.type.equals(RobotType.ARCHON)) && !(enemy.type.equals(RobotType.GARDENER))){
							panicMode = true;
							RobotPlayer.rc.broadcast(3, 1);
							continue;
						}
					}
					*/
					
					// If gardener can't find tree after a long time, just stop and try to plant trees
					int currentRound = RobotPlayer.rc.getRoundNum();
					if (currentRound - roundSpawn == 30){
						moveDirection = moveDirection.rotateRightDegrees(70 + (int)(Math.random() * 10));
					}
					else if (currentRound - roundSpawn > 120){
						treeMode = true;
						int temp = RobotPlayer.rc.readBroadcast(2);
						RobotPlayer.rc.broadcast(2, temp+1);
					}
					
					boolean doPlant = checkHexagon(RobotPlayer.rc.getLocation());
					
					// Gardener can plant at least 5 trees at current location
					if (doPlant){
						treeMode = true;
						int temp = RobotPlayer.rc.readBroadcast(2);
						RobotPlayer.rc.broadcast(2, temp+1);
					}
					
					// If gardener can't plant at least 5 trees at current location, keep moving
					else {
						
						if (RobotPlayer.rc.canMove(moveDirection)){
							RobotPlayer.rc.move(moveDirection);
						}
						
						else{
							moveDirection = moveDirection.rotateRightDegrees(70 + (int)(Math.random() * 10));
						}
						
					}
				}
					
				
				
			}
			
			catch (Exception e) {
				System.out.println("Gardener unhandled exception occured");
				e.printStackTrace();
			}
			
			
		}
		
		
	}
	
	
	static Direction randomDirection() {
        return new Direction((float)Math.random() * 2 * (float)Math.PI);
    }
	
	
	
	// boolean[i] is true if tree can be planted at EAST.leftrotate(60 * i) degrees
	static boolean checkHexagon(MapLocation currentLocation){

		int j = 0;
		
		Direction checkDir = Direction.getEast();
		
		for (int i = 0; i < 6; i++){
			if(RobotPlayer.rc.canPlantTree(checkDir.rotateLeftDegrees(60*i))){
				j++;
			}
		}
		if (j >= 6){
			return true;
		}
		
		return false;
	}
	
	
	
	// Returns the TreeInfo of the tree with the last HP in the passed in TreeInfo[]
	static TreeInfo findWeakestTree(TreeInfo[] nearbyTrees){
		
		float min = nearbyTrees[0].getHealth();
		int j = 0;
		
		for (int i = 0; i < nearbyTrees.length; i++){
			float thisHP = nearbyTrees[i].getHealth();
			if (thisHP < min){
				min = thisHP;
				j = i;
			}
		}
		return nearbyTrees[j];
	}
	
}