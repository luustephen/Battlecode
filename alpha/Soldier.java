package alpha;
import battlecode.common.*;

import java.util.Arrays;
import java.util.Random;

public strictfp class Soldier {
	
	public static RobotInfo[] enemies;
	public static RobotInfo[] allies;
	public static TreeInfo[] trees;
	public static Team team = RobotPlayer.rc.getTeam();
	public static MapLocation location;
	public static MapLocation archonLocation;
	public static boolean shouldMove = true;
	public static Direction moveDirection = randomDirection();
	
	public static void runSoldier() throws GameActionException {
		
		
		while (true) {
			
			try {
				
		        location = RobotPlayer.rc.getLocation();
				archonLocation = new MapLocation(RobotPlayer.rc.readBroadcast(0),RobotPlayer.rc.readBroadcast(1));
				enemies = RobotPlayer.rc.senseNearbyRobots(-1,team.opponent());
				allies = RobotPlayer.rc.senseNearbyRobots(-1,team);
		        
				if(RobotPlayer.rc.readBroadcast(3) != 0 && enemies.length == 0){
					if(RobotPlayer.rc.canMove(archonLocation) && shouldMove) //If soldier should move to enemy archon location, move there
						RobotPlayer.rc.move(archonLocation);
					else if (!(RobotPlayer.rc.canSenseLocation(archonLocation)) && shouldMove){ //If the soldier is not in range of the archon, turn left until it can move then try again.
						Direction dir = new Direction(location,archonLocation);
						dir = dir.rotateLeftDegrees(1);
						while (!(RobotPlayer.rc.canMove(dir))){
							dir = dir.rotateLeftDegrees(1);
						}
						RobotPlayer.rc.move(dir);
						}
					else{ //If the soldier is in range of the archon, move in projector screen saver fashion
						shouldMove = false;
						while (!(RobotPlayer.rc.canMove(moveDirection))){
							moveDirection = moveDirection.rotateLeftDegrees(1);
						}
					
						RobotPlayer.rc.move(moveDirection);
					}
				}
				else{
					if(RobotPlayer.rc.canMove(enemies[0].location)){ //If soldier should move to enemy location, move there
						RobotPlayer.rc.move(enemies[0].location);
						RobotPlayer.rc.fireSingleShot(new Direction(location,enemies[0].location));
					}
					else if (allies.length > 0){
						if(RobotPlayer.rc.canMove(allies[0].location)){
							RobotPlayer.rc.move(allies[0].location);
						}
					}
					else{ //If the soldier is in range of the enemy, move in projector screen saver fashion
						while (!(RobotPlayer.rc.canMove(moveDirection))){
							moveDirection = moveDirection.rotateLeftDegrees(1);
						}
					
					RobotPlayer.rc.move(moveDirection);
					}
				}

		        enemies = RobotPlayer.rc.senseNearbyRobots(-1,team.opponent());
		        RobotPlayer.rc.fireSingleShot(new Direction(location,enemies[0].location));	//Fire at nearest enemy
		        
		        trees = RobotPlayer.rc.senseNearbyTrees(-1, team.NEUTRAL); //If not enemy, shake tree
		        if(RobotPlayer.rc.canShake(trees[0].location))
		        	RobotPlayer.rc.shake(trees[0].location);
			}
			
			catch (Exception e) {
				System.out.println("Soldier unhandled exception occured");
				e.printStackTrace();
			}	
		}
	}

	static Direction randomDirection() {
        return new Direction((float)Math.random() * 2 * (float)Math.PI);
    }
	
	
	
}