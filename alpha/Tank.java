package alpha;
import battlecode.common.*;

import java.util.Arrays;
import java.util.Random;

public strictfp class Tank {
	
	public static RobotInfo[] enemies;
	public static TreeInfo[] trees;
	public static Team team = RobotPlayer.rc.getTeam();
	public static MapLocation location;
	public static boolean shouldMove = true;
	public static MapLocation archonLocation;
	public static Direction moveDirection = randomDirection();
	
	public static void runTank() throws GameActionException {
		
		
		while (true) {
			
			try {
		        location = RobotPlayer.rc.getLocation();
				archonLocation = new MapLocation(RobotPlayer.rc.readBroadcast(0),RobotPlayer.rc.readBroadcast(1));
		        
				if(RobotPlayer.rc.canMove(archonLocation) && shouldMove) //If tank can move towards enemy archon
					RobotPlayer.rc.move(archonLocation);
				else if (!(RobotPlayer.rc.canSenseLocation(archonLocation))){ //If archon is out of range, try to rng out of stuck
					Direction dir = randomDirection();
					while (!(RobotPlayer.rc.canMove(dir))){
			            dir = randomDirection();
			        }
			        RobotPlayer.rc.move(dir);
				}
				else{ //If archon location is reached, dvd move
					shouldMove = false;
					while (!(RobotPlayer.rc.canMove(moveDirection))){
						moveDirection = moveDirection.rotateLeftDegrees(63);
					}
					
					RobotPlayer.rc.move(moveDirection);
				}
				
		        enemies = RobotPlayer.rc.senseNearbyRobots(-1,team.opponent()); //Fire at enemy
		        RobotPlayer.rc.fireTriadShot(new Direction(location,enemies[0].location));
		        
		        trees = RobotPlayer.rc.senseNearbyTrees(-1, team.opponent()); //Fire at enemy trees
		        RobotPlayer.rc.firePentadShot(new Direction(location,trees[0].location));
		        
		        trees = RobotPlayer.rc.senseNearbyTrees(-1, team.NEUTRAL); //Fire at neutral trees
		        RobotPlayer.rc.firePentadShot(new Direction(location,trees[0].location));
		        
			}
			
			catch (Exception e) {
				System.out.println("Tank unhandled exception occured");
				e.printStackTrace();
			}	
		}
	}

	static Direction randomDirection() {
        return new Direction((float)Math.random() * 2 * (float)Math.PI);
    }
	// Returns the compass direction (NESW) that direction parameter is closest to
	static Direction getNESW(Direction direction){
		
		Direction out = Direction.getNorth();
		
		float min = Math.abs(direction.degreesBetween(Direction.getNorth()));
		
		float temp = Math.abs(direction.degreesBetween(Direction.getEast()));
		if (temp < min){
			min = temp;
			out = Direction.getEast();
		}

		temp = Math.abs(direction.degreesBetween(Direction.getSouth()));
		if (temp < min){
			min = temp;
			out = Direction.getSouth();
		}
		
		temp = Math.abs(direction.degreesBetween(Direction.getWest()));
		if (temp < min){
			min = temp;
			out = direction.getWest();
		}
		
		return out;
	}
	
	
}