package alpha;
import battlecode.common.*;

public strictfp class Archon {
	
	public static int numGardeners = 0;

	public static int mismatchCheck = 1;
	
	public static MapLocation archonLocation = RobotPlayer.rc.getInitialArchonLocations(RobotPlayer.rc.getTeam().opponent())[0];
	//public static Direction moveDirection = getNESW(new Direction(RobotPlayer.rc.getLocation(), archonLocation)).rotateLeftDegrees(90);
	//public static Direction spawnDirection = moveDirection.rotateRightDegrees(90);
	public static Direction moveDirection = Direction.getNorth();
	public static Direction spawnDirection = Direction.getEast();
	public static boolean gardenerMode = false;
	
	// Archon will execute this code
	public static void runArchon() throws GameActionException{
		
		// Code that is performed each round is put in this while loop
		while (true){
			
			// Catch unhandled exceptions -- robot will explode
			try {
				
				if(RobotPlayer.rc.getTeamBullets() >= 10000) {
					RobotPlayer.rc.donate(10000);
				}
				
				//RobotPlayer.rc.broadcast(0, (int)archonLocation.x);
				//RobotPlayer.rc.broadcast(1, (int)archonLocation.y);
				
				
				/*
				MapLocation[] broadcasts = RobotPlayer.rc.senseBroadcastingRobotLocations();
				
				for (MapLocation loc : broadcasts){
					RobotPlayer.rc.setIndicatorDot(loc, 124, 252, 0);
				}
				*/
				
				
				int temp = RobotPlayer.rc.readBroadcast(2);
				if (temp != mismatchCheck){
					if (RobotPlayer.rc.canHireGardener(spawnDirection)){
						RobotPlayer.rc.hireGardener(spawnDirection);
						numGardeners++;
						mismatchCheck = temp;
					}
				}	
			
						
					

				
				if (RobotPlayer.rc.canMove(moveDirection)){
					RobotPlayer.rc.move(moveDirection);
				}
				
				else {
					moveDirection = moveDirection.rotateLeftDegrees(63);
				}
				
			
			}
			
			catch (Exception e) {
				System.out.println("Archon unhandled exception occured");
				e.printStackTrace();
			}
			
		}
		
	}
	
	static Direction randomDirection() {
        return new Direction((float)Math.random() * 2 * (float)Math.PI);
    }
	
	
	// Returns the compass direction (NESW) that direction parameter is closest to
		static Direction getNESW(Direction direction){
			
			Direction out = Direction.getNorth();
			
			float min = Math.abs(direction.degreesBetween(Direction.getNorth()));
			
			float temp = Math.abs(direction.degreesBetween(Direction.getEast()));
			if (temp < min){
				min = temp;
				out = Direction.getEast();
			}

			temp = Math.abs(direction.degreesBetween(Direction.getSouth()));
			if (temp < min){
				min = temp;
				out = Direction.getSouth();
			}
			
			temp = Math.abs(direction.degreesBetween(Direction.getWest()));
			if (temp < min){
				min = temp;
				out = direction.getWest();
			}
			
			return out;
		}
	
}